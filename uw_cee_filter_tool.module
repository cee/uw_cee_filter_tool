<?php

/**
* @file
* Code for JSON Filter feature
* @author Stephen Zhao x255zhao@edu.uwaterloo.ca
* @author Gerry Saporito gksapori@edu.uwaterloo.ca
*/

include_once 'uw_cee_filter_tool_json_to_html.inc';
include_once 'uw_cee_filter_tool_json_schema_check.inc';

/**
* Implements hook_wysiwyg_plugin().
* @param $editor
* @param $version
* @return array
*/
function uw_cee_filter_tool_wysiwyg_plugin($editor, $version)
{
  if ($editor === 'ckeditor') {
    return array(
      'filter_tool' => array(
        'path' => drupal_get_path('module', 'uw_cee_filter_tool') . '/plugins/filter_tool',
        'filename' => 'plugin.js',
        'load' => TRUE,
        'buttons' => array(
          'Filter Tool' => t('Filter Tool'),
        ),
      ),
    );
  }
}

// /**
// * Implements hook_preprocess_html();
// * @param $variables
// */
function uw_cee_filter_tool_preprocess_html(&$variables){
  //Add CSS and JS here using drupal_add_css & drupal_add_js()
  drupal_add_css(drupal_get_path('module', 'uw_cee_filter_tool') . '/_uw_cee_filter_tool_css.css');
  drupal_add_js(drupal_get_path('module', 'uw_cee_filter_tool') . '/js/_uw_cee_filter_tool_js.js', 'inline');
}

/**
* Implements hook_filter_info().
*
* Provides filter to convert custom ckfiltertool
* elements to embed objects
*/
function uw_cee_filter_tool_filter_info()
{
  $filters['uw_cee_filter_tool'] = array(
    'title' => t('CKEditor Filter Tool'),
    'description' => t('Converts filter tool elements into widgets.'),
    'process callback' => '_uw_cee_filter_tool_filter_process',
  );
  return $filters;
}

/**
* Implements callback_filter_process()
*
* @description Filter for making http request to google apps script and turning return JSON into HTML output
* @IMPORTANT The filter must be enabled under the desired text formats
* @IMPORTANT ckfiltertool element must be added to exceptions
*/
function _uw_cee_filter_tool_filter_process($text, $filter, $format, $langcode, $cache, $cache_id) {
  // Skip filter process on non node edit pages.
  if (arg(0) == 'node' && (arg(2) == 'edit' || arg(2) == 'moderation')) {
    return;
  }

  //Create error array
  $errors = [];
  //variable that contains the matched outputs
  unset($matches);
  unset($doc);
  // If there aren't any, don't process any further.
  if (!preg_match_all('`</?ck(?:filtertool)\b.*?>`is', $text, $matches)) {
    return $text;
  }
  //Turn $text string into DOM object
  $doc = new DOMDocument;
  //@ operator will return error for malformed HTML
  @$doc->loadHTML($text);
  //Search for the ckfiltertool element
  $elements = $doc->getElementsByTagName('ckfiltertool');
  //Setting up all html for the page(in case there are multiple filters they'll add to the same page)
  $page_html = "";
  //loop through each instance of ckfiltertool
  foreach ($elements as $element) {
    //variable to contain the output html or blank to censor the apps script link
    unset($html);
    //get Google sheet link of ckfiltertool element
    $gslink = $element->getAttribute('data-filtertool-link');
    //Checking if attribute matches RegEx
    if (!preg_match('/(https?:\/\/(script\.)?google(usercontent)?\.com)/', $gslink)) {
      array_push($errors, 'Invalid Apps Script URL.');
      //Checking that the filter_tool module exists
    } else {
      //send http request
      $output = drupal_http_request($gslink); //HTTP call returns object
      if (isset($output->{'error'})) {
        array_push($errors, 'A error occurred when connecting to google apps script.' . $output->{'error'});
      } else {
        //parsing json data from string to array and checking for errors
        //Custom function from .inc file.
        $result = json_validate($output->{'data'});
        //Checking if there are any errors with the JSON project
        if (!$result['error'] === '') {
          array_push($errors, $result['error']);
        } else {
          //Parse JSON into HTML DOM string.
          //Custom function from .inc file.
          $html = json_to_html($result['data']);
          $page_html .= "<span>".$html."</span>";
        }
      }
    }
    //Printing errors
    foreach ($errors as $error) {
      drupal_set_message($error, 'error', FALSE);
    }
  }
  //Adding in the new HTML
  $pattern = '/((<p>)?<ckfiltertool data\-filtertool\-link\=\"https\:\/\/script\.google(usercontent)?\.com\/macros\/(\w)?(echo\?user_content_key\=)?([^\>]*)\"\>)/';
  $new_text = preg_replace($pattern, $page_html, $text);
  return $new_text;
}
