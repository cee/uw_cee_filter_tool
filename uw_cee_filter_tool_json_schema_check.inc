<?php
/**
 * @author Stephen Zhao x255zhao@edu.uwaterloo.ca
 * @author Gerry Saporito gksapori@edu.uwaterloo.ca
 */
/** Checks JSON schema
 * @param $string
 * @return mixed
 */
function json_validate($string) {
    strip_tags($string);
    if (strpos($string, "[") !== false || strpos($string, "]" !== false)) {
        exit('JSON should not contain arrays for security.');
    }
    // decode the JSON data
    $result['data'] = json_decode($string, true);
    $result['error'] = '';

    // switch and check possible JSON errors
    switch (json_last_error()) {
        case JSON_ERROR_NONE:
            $error = ''; // JSON is valid // No error has occurred
            break;
        case JSON_ERROR_DEPTH:
            $error = 'The maximum stack depth has been exceeded.';
            break;
        case JSON_ERROR_STATE_MISMATCH:
            $error = 'Invalid or malformed JSON.';
            break;
        case JSON_ERROR_CTRL_CHAR:
            $error = 'Control character error, possibly incorrectly encoded.';
            break;
        case JSON_ERROR_SYNTAX:
            $error = 'Syntax error, malformed JSON.';
            break;
        // PHP >= 5.3.3
        case JSON_ERROR_UTF8:
            $error = 'Malformed UTF-8 characters, possibly incorrectly encoded.';
            break;
        // PHP >= 5.5.0
        case JSON_ERROR_RECURSION:
            $error = 'One or more recursive references in the value to be encoded.';
            break;
        // PHP >= 5.5.0
        case JSON_ERROR_INF_OR_NAN:
            $error = 'One or more NAN or INF values in the value to be encoded.';
            break;
        case JSON_ERROR_UNSUPPORTED_TYPE:
            $error = 'A value of a type that cannot be encoded was given.';
            break;
        default:
            $error = 'Unknown JSON error occurred.';
            break;
    }

    if ($error !== '') {
        // throw the Exception or exit
        $result['error'] = $error;
        return $result;
    } else {
        $error = json_schema_check($result);
        if ($error !== '') {
            // throw the Exception or exit
            $result['error'] = $error;
            return $result;
        }
    }
    // everything is OK
    return $result;
}

/**Check schema specific errors
 * @param $result array from json string
 * @return string error message
 */
function json_schema_check($result)
{
    //Recursively goes through array to encode illegal characters
    array_walk_recursive($result, 'jsoncallback');

    $keyarray = [];
    foreach ($result as $key => $product) {
        //Product must have unique ID
        if (in_array($key, $keyarray)) {
            return "Each product ID must be unique. {$key} is not unique.";
        }
        //DefaultCategory must exist for every product
        if (!array_key_exists("DefaultCategory", $product)) {
            return "The DefaultCategory key not found and is required. {$key} does not contain DefaultCategory.";
        }
        foreach ($product as $type => $info) {
            //sorts key values for comparison
            $index = sort(array_keys($info), SORT_STRING);
            switch (explode("_", $type)[0]) {
                case "Link":
                    if ($index != ["alttext", "bold", "hidden", "href", "target", "filter"]) {
                        return "The link element must only contain: bold\",\"href\",\"alttext\",\"target\",\"hidden\". {$key}->{$type} does not comply.";
                    }
                    if (!is_bool($info["hidden"])) {
                        return "The hidden attribute must be a boolean. {$key}->{$type} does not comply.";
                    }
                    if(!is_bool($info["filter"])) {
                        return "The filter attribute must be a boolean. {$key}->{$type} does not comply.";
                    }
                    break;
                case "Image":
                    if ($index != ["alt", "hidden", "src", "hidden"]) {
                        return "The Photo element must only contain: \"src\",\"alt\",\"hidden\". {$key}->{$type} does not comply. ";
                    }
                    if (!is_bool($info["hidden"])) {
                        return "The hidden attribute must be a boolean. {$key}->{$type} does not comply.";
                    }
                    if(!is_bool($info["filter"])) {
                        return "The filter attribute must be a boolean. {$key}->{$type} does not comply.";
                    }
                    break;
                case "Category":
                    if (!array_key_exists("hidden", $info)) {
                        return "Category must contain: \"hidden\". {$key}->{$type} does not comply.";
                    }
                    if (!array_key_exists("filter", $info)) {
                        return "Category must contain: \"filter\". {$key}->{$type} does not comply.";
                    }
                    if (!is_bool($info["hidden"])) {
                        return "The hidden attribute must be a boolean. {$key}->{$type} does not comply.";
                    }
                    if(!is_bool($info["filter"])) {
                        return "The filter attribute must be a boolean. {$key}->{$type} does not comply.";
                    }
                    break;
            }
        }
    }
    return '';
}

/**
* @param $item
* @param $key
*/
function jsoncallback(&$item,&$key){
  $item = check_plain($item);
  $key = check_plain($key);
}
