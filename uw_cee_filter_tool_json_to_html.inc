<?php

/**
* @file
* uw_cee_filter_tool_json_to_html.inc
* contains custom functions to parse JSON into HTML
* @author Stephen Zhao x255zhao@edu.uwaterloo.ca
* @author Gerry Saporito gksapori@edu.uwaterloo.ca
*/

/** Takes decoded JSON and outputs into html
* @param $json_array
* @return string
*/
function json_to_html(&$json_array) {
  $catProps = array();
  $default = [];

  $html_out = "<div id=\"filter-start\">\n";

  //Categorizing into default category
  foreach ($json_array as $key => $value) {
    $default[$value['DefaultCategory']][$key] = $value;
  }

  //sorting
  ksort($default);//Sorts default categories in ascending order
  foreach ($default as $value) {
    ksort($value);//sorts products in ascending order
  }

  //Categorizing into filter categories
  foreach ($json_array as $jsonArr => $arrTitle) {
    foreach($arrTitle as $indexParam => $indexTitle){
      $bool = false;
      if($indexParam == "DefaultCategory"){
        continue;
      } elseif($indexParam == "Category") {
        foreach($indexTitle as $paramTitle => $paramValues){
          //searches for category title
          foreach($paramValues as $filter => $boolean){
            if($filter === "filter" && $boolean) {
              $bool=true;
            }
          }
          //inserts category title in array if not in array
          if($bool == true && !array_key_exists($paramTitle, $catProps)) {
            $catProps[$paramTitle] = array();
          }
          //inserts category option in array if not in array already
          foreach($paramValues as $filter => $boolean){
            $newParamValues = explode(", ", $paramValues[0]);
            if(array_key_exists($paramTitle, $catProps)) {
              if(is_string($newParamValues)){ //checks and inserts for a single parameter
                if($filter === 'filter' && $boolean === true && !in_array($paramValues[0], $catProps[$paramTitle])){
                  array_push($catProps[$paramTitle], $paramValues[0]);
                }
              } else { //breaks up string with multiple aprameters and inserts if not already in filter array
                foreach($newParamValues as $string) {
                  $newStr = str_replace('and ', '', $string);
                  if($filter === 'filter' && $boolean === true && !in_array($newStr, $catProps[$paramTitle])){
                    array_push($catProps[$paramTitle], $newStr);
                  }
                }
              }
            }
          }
          //reset bool
          $bool = false;
        }
      } else {
        continue;
      }
    }
  }

  //Sorting $catProps
  foreach($catProps as $key => $value) {
    sort($catProps[$key]);
  }

  //Filter Parameters
  $html_out .= "<div id='filter filterBegin'>\n";

  //Filter Buttons
  $html_out .= "<p id='filterBy-text'>Filter By...</p>\n";
  foreach ($catProps as $select => $param) {
    $html_out .= "<select>\n<option value='{$select}' disabled selected>{$select}</option>\n";
    foreach($param as $option) {
      $html_out .= "<option value='{$option}'>{$option}</option>\n";
    }
    $html_out .= "</select>\n";
  }

  //Search Box
  $html_out .= "<div id='filter'>\n";
  $html_out .= "<div class='button_box2'>\n<div class='form-wrapper-2 cf'>\n";
  $html_out .= "<input id='search' autocomplete='off' type='text' placeholder='Search Here...' required>\n";
  $html_out .= "<button id='searchBtn'>Search</button>\n</div>\n</div>\n";

  //Clear button
  $html_out .= "<button id='clear'>Clear</button>\n</div>\n";

  $html_out .= "</div>\n";

  //Converting JSON to html elements
  foreach ($default as $defaultcategory => $value) {//each default category
    if(!preg_match('/^</', $defaultcategory)){
      $html_out .= "<div class=\"filter-defaultcategory\">\n<h2>{$defaultcategory}</h2>\n";
      $html_out .= "<div id='masonry'>\n";
      foreach ($value as $uniqueid => $product) {//each product
        $html_out .= "<div class=\"product-vert filter-product-{$uniqueid}\">\n";
        $html_out .= "<div class=\"text \">\n";
        foreach ($product as $key => $field) {//each field
          $html_out .= array_to_html($key, $field, true); //outputs html
        }
        $html_out .= "</div>\n\n";

        //Button
        $html_out .= "<button class='button-vert modalBtn'>Learn More</button>\n";
        //Pop-up Window (modal)
        $html_out .= "<div class='modal-window'>\n";
        $html_out .= "<div class='modal-content'>\n<span class='close'>&times;</span>\n";
        $html_out .= "<h2>{$product['Name']['text']}</h2>\n";
        $html_out .= "<div class='contentDiv'>\n";
        //Modal Content
        foreach ($product as $key => $field) {//each field
          $html_out .= array_to_html($key, $field, false); //outputs html
        }
        $html_out .= "</div>\n</div>\n</div>\n</div>\n";
      }

      $html_out .= "</div>\n<hr>\n</div>\n\n";
    }
  }
  $html_out .= "</div>\n";

  return $html_out;
}

/**
* @param $input_key
* @param $input_array
* @return string
*/
function array_to_html($input_key, $input_array, $input_hide)
{
  $output = '';
  switch (explode("_", $input_key)[0]) {
    case "DefaultCategory":
    break;
    case "Name":
    if($input_hide == true){
      $output .= "<h4>{$input_array['text']}</h4>\n";
    } else {
      continue;
    }
    break;
    case "Link": //Case for link to make button in modal
    if($input_hide == false) {
      if(strpos($input_array['href'], '@')){
        $output .= "<a href=\"mailto:{$input_array['href']}\" target= \"{$input_array['target']}\" class='button'>{$input_array['bold']}</a>\n";
      } else if ($input_array['href']) {
        $output .= "<a href=\"{$input_array['href']}\" target= \"{$input_array['target']}\" class='button'>{$input_array['bold']}</a>\n";
      }
    }
    break;
    case "Image": //Case for photo on thumbnail
    if($input_array['src']){
      if ($input_hide == true) {
        $output .= "<img class='vertImg' src=\"{$input_array['src']}\" alt=\"{$input_array['alt']}\">\n";
      } else { //Case for major display in modal
        $output .= "<img class='horImg' src=\"{$input_array['src']}\" alt=\"{$input_array['alt']}\">\n";
      }
    }
    break;
    case "Category":
    if($input_hide) { //if outputting to the thumbnail (therefore hiding most input)
      //checks to see if option is hidden
      foreach($input_array as $option => $data){
        $hide = true;
        foreach($data as $param => $value){
          if($param === "hidden" && $value === false){
            $hide = false;
          }
        }
        //if it is shown on thumbnail, output
        if($hide === false){
          $output .= "<p><strong>{$option}: </strong>{$data[0]}</p>\n";
        }
      }
    } else { //otherwise outputting all input into table in modal
      $output .= "<table class='lowerTable'>\n";
      foreach($input_array as $option => $data){
        $output .= "<tr>\n<td class='title'><strong>{$option}</strong></td>\n";
        $output .= "<td class='info'>{$data[0]}</td>\n</tr>\n";
      }
      $output .= "</table>\n";
    }
    break;
  }
  return $output;
}

function checkForDollarSigns($var) {
  if(preg_match("/\s\$/", $var)) {
    return preg_replace("/\s\$/", " HELLO WORLD!!!!", $var);
  }
}
