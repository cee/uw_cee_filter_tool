# UW CEE Filter Tool
This tool uses google apps script to parse through google sheets data and assemble a JSON.
JSON is then passed to Drupal to be formatted into HTML.
## Setup
Don't upload the google_sheets, html_test_files, and test_files directories
1. Install the uw_cee_filter_tool module
2. Enable the wysiwyg plug-in at https://d7/fdsu1/admin/config/content/wysiwyg
3. Go to https://d7/fdsu1/admin/config/content/formats and enable the
**CkEditor Filter Tool** and add **ckfiltertool[data-filtertool-link]** to the WYSIWYG filtertool
4. Change the filter order so the filter tool has the highest weight, at https://d7/fdsu1/admin/config/content/formats

## Use
1. Set-up apps script web app or use examples in /google_sheets/link.txt or google_sheets/uw_cee_filter_tool_google_appscript_template.js
2. Go to wysiwyg and click the filter tool icon
3. Enter in google apps script url
4. Hit save
