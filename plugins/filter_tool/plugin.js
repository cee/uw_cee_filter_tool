/**
 * @file
 * @author Stephen Zhao x255zhao@edu.uwaterloo.ca
 */

cktags = ['ckfiltertool'];

var doubleclick_element;

CKEDITOR.plugins.add('filter_tool', {
    required: ['dialog', 'fakeobjects'],
    init: function (editor) {
        var pluginName = 'filter_tool';
        //set up object to share variables
        CKEDITOR.filter_tool = {};

        //Register button for Filter Tool
        editor.ui.addButton('Filter Tool', {
            label: "Add/Edit Filter Tool",
            command: 'filtertool',
            icon: this.path + 'icons/filter_tool.png',
        });

        //Register right click menu item for Call to Action
        editor.addMenuItems({
            filter_tool: {
                label: "Edit Filter Tool",
                icon: this.path + 'icons/filter_tool.png',
                command: 'filtertool',
                group: 'image',
                order: 1
            }
        });

        //Make ckfiltertool a self closing tag
        CKEDITOR.dtd.$empty['ckfiltertool'] = 1;
        //Make sure the fake element for Filter tool has a name
        CKEDITOR.filter_tool.ckfiltertool = 'filtertool';
        CKEDITOR.lang.en.fakeobjects.ckfiltertool = CKEDITOR.filter_tool.ckfiltertool;
        //Add javascript file that defines the dialog box for Filter tool
        CKEDITOR.dialog.add('filtertool', this.path + 'dialogs/filter_tool.js');
        // Add command to open dialog box when button is clicked
        editor.addCommand('filtertool', new CKEDITOR.dialogCommand('filtertool'));

        //reuglar expressions for filter tool
        CKEDITOR.filter_tool.filtertool_regex = /(https?:\/\/(script\.)?google(usercontent)?\.com)/;
        //Open the appropriate dialog box if an element is double-clicked
        editor.on('doubleclick', function (evt) {
            var element = evt.data.element;

            //store the element as global variable
            doubleclick_element = element;

            if (element.is('img') && element.data('cke-real-element-type') === 'ckfiltertool') {
                evt.data.dialog = 'filtertool';
            }
        });
        //Adding the right-click menu if an element is right-clicked.
        if (editor.contextMenu) {
            editor.contextMenu.addListener(function (element, selection) {
                if (element && element.is('img') && element.data('cke-real-element-type') === 'ckfiltertool') {
                    return {filtertool: CKEDITOR.TRISTATE_OFF}
                }
            });
        }

        //Add cs in editor to style fake elemnts.
        CKEDITOR.addCss(
            'img.ckfiltertool {' +
            'background-image: url(' + CKEDITOR.getUrl(this.path + 'icons/big/filter_tool.png') + ');' +
            'background-position: center center;' +
            'background-repeat: no-repeat;' +
            'background-color: #000;' +
            'width: 100%;' +
            'height: 100px;' +
            'margin: 0 0 10px 0;' +
            'margin-left: auto;' +
            'margin-right: auto;' +
            '}' +

            // Call to Action: Definitions for wide width.
            '.uw_tf_standard_wide p img.filter_tool {' +
            'height: 200px;' +
            '}' +

            // Call to Action: Definitions for standard width.
            '.uw_tf_standard p img.filter_tool {' +
            'height: 200px;' +
            '}'
        );
    },
    afterInit: function (editor) {
        //Make the fake image display on first load/return from source view
        if (editor.dataProcessor.dataFilter) {
            editor.dataProcessor.dataFilter.addRules({
                elements: {
                    ckfiltertool: function (element) {
                        //Reset title
                        CKEDITOR.lang.en.fakeobjects.ckfiltertool = CKEDITOR.filter_tool.ckfiltertool;
                        //Adjust title if a list name is present
                        if (element.attributes['filtertool-link']) {
                            CKEDITOR.lang.en.fakeobjects.ckfiltertool += ': ' + element.attributes['filtertool-link'];
                        }
                        //Just returns whatever attributes are on the element; may want to filter those
                        return editor.createFakeParserElement(element, 'ckfiltertool', 'ckfiltertool', false);
                    }
                }
            });
        }
    }
});
