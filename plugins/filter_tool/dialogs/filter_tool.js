/**
 * @file
 * Dialog for filter tool
 * @author Stephen Zhao x255zhao@edu.uwaterloo.ca
 */

(function () {
    //from submission tracking variable
    var formSubmitted = false;
    var filtertoolDialog = function (editor) {
        return {
            title: 'Filter Tool Properties',
            minWidth: 625,
            minHeight: 150,
            contents: [{
                id: 'filtertool',
                label: 'filtertool',
                elements: [{
                    type: 'text',
                    id: 'filtertool-link',
                    label: 'Please enter Google Sheets Link: ',
                    required: true,
                    setup: function (element) {
                        this.setValue(element.getAttribute('data-filtertool-link'));
                    }
                }]
            }],
            onOk: function () {
                //get form info.
                filtertool_link = this.getValueOf('filtertool', 'filtertool-link');

                //Validate the input.
                if (!filtertool_link) {
                    errors = "You must enter in a Google Apps Script Link.\r\n";
                } else {
                    errors = '';
                }
                if (!CKEDITOR.filter_tool.filtertool_regex.test(filtertool_link)) {
                    errors += "You must enter a valid Google Apps Script url. \r\n";
                }

                // If form has been submitted before then set it back to not being seeing before.
                // i.e if this is double submission set it back to not being run before.
                if (formSubmitted == true) {
                    formSubmitted = false;
                    return false;

                // Only display erros if there are errors to display and the form has not been run before.
                } else if (errors && formSubmitted == false) {
                    alert(errors);
                    formSubmitted = true;
                    return false;
                } else {
                    //Crete the facts/figures element
                    var ckfiltertoolNode = new CKEDITOR.dom.element('ckfiltertool');
                    //Save the contents of dialog as an attribute of the element
                    ckfiltertoolNode.setAttribute('data-filtertool-link', filtertool_link);
                    //Adjust the title based on user input
                    CKEDITOR.lang.en.fakeobjects.ckfiltertool = CKEDITOR.filter_tool.ckfiltertool + ': ' + filtertool_link;
                    //Create fake image for this element and insert into editor
                    var newFakeImage = editor.createFakeElement(ckfiltertoolNode, 'ckfiltertool', 'ckfiltertool', false);
                    //set the fake image to have class to apply CSS
                    newFakeImage.addClass('ckfiltertool');
                    if (this.fakeImage) {
                        newFakeImage.replace(this.fakeImage);
                        editor.getSelection().selectElement(newFakeImage);
                    } else {
                        editor.insertElement(newFakeImage);
                    }
                    //reset Tittle
                    CKEDITOR.lang.en.fakeobjects.ckfiltertool = CKEDITOR.filter_tool.ckfiltertool;
                }
            },
            onShow: function () {
                //Set up to handle existing items
                this.fakeImage = this.ckfiltertoolNode = null;
                var fakeImage;

                //Check if element is right clicked or icon was clicked, if not use global variable double click
                if (this.getSelectedElement()) {
                    fakeImage = this.getSelectedElement();
                } else {
                    fakeImage = fakeImage;
                    console.log(fakeImage);
                }

                if (fakeImage && fakeImage.data('cke-real-element-type') && fakeImage.data('cke-real-element-type') === 'ckfiltertool') {
                    this.fakeImage = fakeImage;
                    var ckfiltertoolNode = editor.restoreRealElement(fakeImage);
                    this.ckfiltertoolNode = ckfiltertoolNode;
                    this.setupContent(ckfiltertoolNode);
                }
            }
        }
    };

    CKEDITOR.dialog.add('filtertool', function (editor) {
        return filtertoolDialog(editor);
    });
})();
