<?php
$json = '{
  "1":{
    "DefaultCategory":"Award",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/nsercnaturalsciencesandengineeringresearchcouncilofcanadalogo.jpg",
      "alt":"Natural Sciences and Engineering Resources Canada, Government of Canada Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"NSERC Experience Award",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Award",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"Up to $4500",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"Canada",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"At least 4 weeks before the student’s start date",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"No",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Undergraduate",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"Private companies that hire undergraduate researchers.",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"Apply through their website",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Research",
        "1":" Materials, Manufacturing, Processing and Systems Engineering",
        "2":" Computing: Hardware & Software, Information Systems, and Security",
        "3":" AI, Robotics and Data Science",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"http://www.nserc-crsng.gc.ca/Students-Etudiants/UG-PC/Experience-Experience_eng.asp",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "2":{
    "DefaultCategory":"Grant",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/albertainnovateslogo.png",
      "alt":"Alberta Innovates Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Alberta Innovates Health Solutions (AIHS)",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Grant",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"Varies depending on the program",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"Alberta",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"Varies depending on the program",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"No",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Any",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"Businesses in British Columbia that run programs that support health research and innovation activities. These activities must be focused on enhancing the effectiveness of the health system, improving health outcomes, and ensuring that research translates into technologies, tools, and policies for better health. There are a wide variety of programs that businesses can apply to, depending on the nature of their project.",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"Visit the website, browse all available funding opportunities, and submit an application to your program of choice.",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Health Care",
        "1":" Ergonomics, Sports & Fitness, Occupational Health",
        "2":"  Research",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"https://albertainnovates.ca/funding-health-innovations/health-research-funding/",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "3":{
    "DefaultCategory":"Grant",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/msfhrmichaelsmithfoundationforhealthresearchlogo.png",
      "alt":"Michael Smith Foundation for Health Research Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"MSFHR Funding Programs",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Grant",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"Varies",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"British Columbia",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"Varies",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"No",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Graduate ",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"Health researchers in BC",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"Via their website",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Health Care",
        "1":" Ergonomics, Sports & Fitness, Occupational Health",
        "2":" ",
        "3":" Research",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"https://www.msfhr.org/funding",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "4":{
    "DefaultCategory":"Grant",
    "Image":{
      "src":"",
      "alt":" Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Western Innovation Initiative",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Grant",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"50% of eligible project costs up to $3.5 mil",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"British Columbia, Alberta, Saskatchewan, Manitoba",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"Late November",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"No",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Any",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"be incorporated to conduct business in Canada, be less than 500 full-time equivalent (FTE) employees (i.e., SMEs), be in operation for at least one year, have operating facilities located in Western Canada, have a viable plan to commercialize (generate revenues from sales) an innovative, technology-driven, new or improved product, service or process within three years of project commencement, have a viable plan to commercialize (generate revenues from sales) an innovative, technology-driven, new or improved product, service or process within three years of project commencement, have confirmed, at the time of application, funding from non-governmental sources that should total at least 50 percent of the proposed project costs",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"Visit website for contact information",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Any",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"http://www.wd-deo.gc.ca/eng/14857.asp",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "5":{
    "DefaultCategory":"Grant",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/nationalresearchcouncilcanadalogo.jpg",
      "alt":"National Research Council Canada, Government of Canada Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Youth Employment Program - Youth Green",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Grant",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"Covers a portion of intern salary",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"Canada",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"n/a",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"Yes",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Undergraduate",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"Incorporated for-profits with 500 or less employees that aligns with green sectors or industries or internship has environmental focus",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"Contact NRC IRAP",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Geomatics & GIS",
        "1":" Environmental and Natural Resource Management, and Sustainability",
        "2":" Mining, Energy Production, and Distribution",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"https://www.nrc-cnrc.gc.ca/eng/irap/services/youth_initiatives.html",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "6":{
    "DefaultCategory":"Grant",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/canadianheritagelogo.jpg",
      "alt":"Canadian Heritage, Government of Canada Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Young Canada Works in Both Official Languages",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Grant",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"Wage subsidies",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"Canada",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"February",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"Yes",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Any",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"A private, public, non-profit organization, or a municipality; incorporated; involved in national, provincial, territorial, municipal or community activities; willing to hire young people from other regions of Canada; conducting activities in both official languages; and stable and financially healthy.",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"Submit through their system",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Any",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"https://www.canada.ca/en/canadian-heritage/services/funding/young-canada-works/employers/official-languages-employers.html",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "7":{
    "DefaultCategory":"Grant",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/canadianheritagelogo.jpg",
      "alt":"Canadian Heritage, Government of Canada Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Young Canada Works in Heritage Organizations",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Grant",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"Wage subsidies",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"Canada",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"January",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"No",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Any",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"An incorporated, non-profit organization in Canada with a heritage mandate such as a museum, archives, a library, or an organization managing a heritage site/built heritage; an educational or cultural institution; a non-profit organization under a provincial, territorial, regional or municipal government; a professional heritage service organization; an Indigenous regional government or governing body and/or Indigenous organization with a mandate to preserve and support Indigenous heritage with distinct objectives, programs and a budget related to heritage",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"Submit through their system",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Tourism, Arts & Entertainment, Recreation & Hospitality",
        "1":" Policy, Legal, Public Service & Government Relations",
        "2":" Teaching and Educational Services",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"https://www.canada.ca/en/canadian-heritage/services/funding/young-canada-works/employers/heritage-organizations-employers.html",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "8":{
    "DefaultCategory":"Grant",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/canadianinstituteofhealthresearchlogo.jpg",
      "alt":"Canadian Institutes of Health Research, Government of Canada Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Project Grant",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Grant",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"Varies",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"Canada",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"Ongoing?",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"No",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Any",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"Researchers at any career stage to build and conduct health-related research and knowledge translation projects",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"Submit registration and 10-page application including CV, then face-to-face committee-based peer review process. ",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Health Care",
        "1":" Ergonomics, Sports & Fitness, Occupational Health",
        "2":" Research",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"http://www.cihr-irsc.gc.ca/e/49051.html",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "9":{
    "DefaultCategory":"Grant",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/mentorworkslogo.png",
      "alt":"Mentor Works LTD Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Canadian Business Grants for Hiring: Connect Canada Internships",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Grant",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"$5,000 from the partnership company, $5,000 from Connect Canada",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"Canada",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"Ongoing",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"No",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Graduate ",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"Located in Canada, for profit and incorporated",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Any",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"https://www.mentorworks.ca/blog/government-funding/connect-canada-05-2014/",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "10":{
    "DefaultCategory":"Grant",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/mitacslogo.png",
      "alt":"Mitacs Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Mitacs Accelerate",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Grant",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"Matching funds up to $7500 for internships of at least 4 months. Additional funding available if the business leverages 3+ interns and 3+ internships.",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"Canada",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"Ongoing",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"Yes",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Graduate ",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"Businesses hiring Masters and PhD students to provide innovative research and develop tools, models, technology, or solutions to research challenges.",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"Fill out application form on website.",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Any",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"http://www.mitacs.ca/en/programs/accelerate#business",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "11":{
    "DefaultCategory":"Grant",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/nationalresearchcouncilcanadalogo.jpg",
      "alt":"National Research Council Canada, Government of Canada Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"National Research Council Canada Industrial Research Assistance Program",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Grant",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"Up to $30,000 for a graduate students salary",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"Canada",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"Ongoing",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"No",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Graduate ",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"Canadian manufacturing companies with the objective of growing and generating profits through the development and commercialization of innovative, technology-driven, new or improved products, services, or processes in Canada.",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"Visit the website to contact a representative and determine your eligibility.",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Materials, Manufacturing, Processing and Systems Engineering",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"https://www.nrc-cnrc.gc.ca/eng/irap/services/financial_assistance.html",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "12":{
    "DefaultCategory":"Grant",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/employmentandsocialdevelopmentcanadalogo.jpg",
      "alt":"Employment and Social Development Canada, Government of Canada Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Canada Summer Jobs",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Grant",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"Public and private sector businesses are eligible for 50% wage coverage, non-for-profit organizations are eligible for 100% wage coverage.",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"Canada",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"Early February",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"No",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Undergraduate",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"Employers from the non-for-profit, public, and private sector may apply if they’re hiring a post-secondary student during the summer for 6 – 16 weeks.",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"Visit the website and fill out an application form.",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Any",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"https://www.canada.ca/en/employment-social-development/services/funding/canada-summer-jobs.html",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "13":{
    "DefaultCategory":"Grant",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/mentorworkslogo.png",
      "alt":"Mentor Works LTD Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Hiring Grants and Wage Subsidies",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Grant",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"Varies",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"Canada",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"Ongoing",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"No",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Alumni",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"All employers. There are a variety of hiring grants and wage subsidies available in the database to support the hiring of new employees who have graduated from a post-secondary institution.",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"Visit the website and fill out an application form.",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Any",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"http://web.mentorworks.ca/find-hiring-grants",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "14":{
    "DefaultCategory":"Grant",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/governmentofontariologo.jpg",
      "alt":"Government of Ontario Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Summer Experience Program",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Grant",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"Up to $3658 per student",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"Canada",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"Early March",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"No",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Undergraduate",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"Non-for-profit organizations, municipalities, Indigenous organizations, and First Nation communities who want to create meaningful summer employment opportunities for students. Jobs must be support either: the Ministry of Citizenship and Immigration, the Ministry of Tourism, Culture and Sport, the Ministry of the Status of Women, or the Ministry of Seniors Affairs.",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"Visit the website and reach out to a Ministry contact to determine your eligibility.",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Tourism, Arts & Entertainment, Recreation & Hospitality",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"http://www.grants.gov.on.ca/GrantsPortal/en/OntarioGrants/GrantOpportunities/OSAPQA005131",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "15":{
    "DefaultCategory":"Grant",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/careerlauncherlogo.png",
      "alt":"Career Launcher Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Clean Tech Internship Program",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Grant",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"70% of an intern’s salary up to $15,000",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"Canada",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"Ongoing",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"No",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Any",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"Canadian companies in the clean tech industry hiring interns for science, technology, engineering, or mathematics fields. Interns must be no more than 30 years of age at the start of the internship and work for 6 – 12 months. The internship must be related to climate change or environmental challenges.",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"Visit the website and submit an internship.",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Materials, Manufacturing, Processing and Systems Engineering",
        "1":" Computing: Hardware & Software, Information Systems, and Security",
        "2":" AI, Robotics and Data Science",
        "3":" Geomatics & GIS",
        "4":" Environmental and Natural Resource Management, and Sustainability",
        "5":" Mining, Energy Production, and Distribution",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"https://careerlauncher.collegesinstitutes.ca/cleantech/",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "16":{
    "DefaultCategory":"Grant",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/careerlauncherlogo.png",
      "alt":"Career Launcher Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Natural Resources Internship",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Grant",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"50% of an intern’s salary up to $15,000",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"Canada",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"Ongoing",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"No",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Any",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"Canadian companies in the natural resource sectors hiring interns for science, technology, engineering, or mathematics fields. Interns must be no more than 30 years of age at the start of the internship and work for 6 – 12 months. The internship must be related to processes with positive environmental outcomes or protecting the environment.",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"Visit the website and submit an internship.",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Materials, Manufacturing, Processing and Systems Engineering",
        "1":" Computing: Hardware & Software, Information Systems, and Security",
        "2":" AI, Robotics and Data Science",
        "3":" Geomatics & GIS",
        "4":" Environmental and Natural Resource Management, and Sustainability",
        "5":" Mining, Energy Production, and Distribution",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"https://careerlauncher.collegesinstitutes.ca/naturalresources/",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "17":{
    "DefaultCategory":"Grant",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/sustainabledevelopmenttechnologycanadalogo.png",
      "alt":"Sustainable Development Technology Canada Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Sustainable Development Technology Canada",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Grant",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"Up to 33% of eligible project costs",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"Canada",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"Ongoing",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"No",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Any",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"Canadian small and medium size enterprises advancing innovative technologies that are pre-commercial and have the potential to demonstrate significant and quantifiable environmental and economic benefits in one or more of the following areas: climate change, clean air, clean water, and clean soil.",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"Visit the website and submit an application form to the fund of your choice.",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Computing: Hardware & Software, Information Systems, and Security",
        "1":" AI, Robotics and Data Science",
        "2":" Geomatics & GIS",
        "3":" Environmental and Natural Resource Management, and Sustainability",
        "4":" Mining, Energy Production, and Distribution",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"https://www.sdtc.ca/en/apply/funds",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "18":{
    "DefaultCategory":"Grant",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/canadiannursesfoundationlogo.png",
      "alt":"Canadian Nurses Foundation Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Canadian Nurses Foundation Research Grants",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Grant",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"$5,000 to $50,000",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"Canada",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"Ongoing",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"No",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Any",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"pilot studies and research projects on nursing care issues that builds research capacity and improves patient or client outcomes",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"Visit the website to apply",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Research",
        "1":" Health Care",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"http://cnf-fiic.ca/what-we-do/research/",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "19":{
    "DefaultCategory":"Grant",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/miningindustryhumanresourcescouncillogo.jpg",
      "alt":"Mining Industry Human Resources Council Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"MiHRs Green Jobs Program",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Grant",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"50% of an employee’s salary, to a maximum of $12,000",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"Canada",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"Ongoing",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"No",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Alumni",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"Small, medium or large companies involved in the mining or mineral exploration sector; Post-secondary educational institution, or; Not-for profit organization.",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":" Apply online",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Geomatics & GIS",
        "1":" Environmental and Natural Resource Management, and Sustainability",
        "2":" Mining, Energy Production, and Distribution",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"https://www.mihr.ca/careers/wage-subsidy-programs/green",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "20":{
    "DefaultCategory":"Grant",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/governmentofmanitobalogo.png",
      "alt":"Government of Manitoba Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Manitoba Job Grant",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Grant",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"$10,000 for each individual employee or prospective employee up to $100,000",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"Manitoba",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"Ongoing",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"No",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Any",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"Participating employers must have a job available for the trainee upon successful completion of the training. For existing employees, the purpose of training should be to upgrade their skills so they may move up to a better job or meet employer needs in the workplace. Employers that receive funding under the Canada-Manitoba Job Grant must contribute a minimum one-third of the eligible training costs. Small businesses (50 employees or less) may be eligible for additional funding support. Training participants must be individual working in Manitoba who are Canadian citizens or permanent residents.",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"Visit website for contact information",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Any",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"http://www.gov.mb.ca/wd/ites/is/cjg.html",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "21":{
    "DefaultCategory":"Grant",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/governmentofmanitobalogo.png",
      "alt":"Government of Manitoba Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Growing Value – Commercialization",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Grant",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"50% for eligible expenses including capital expenses up to $100,000",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"Manitoba",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"Ongoing",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"No",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Any",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"Existing agriproduct processor or a new entrant who intends to operate as an agriproduct processor. Applicants must be in compliance with all Manitoba and Federal Government laws and regulations applicable to their operations. Applicants must also have an AccessManitoba client identification number",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"Visit website for contact information",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Any",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"http://www.gov.mb.ca/agriculture/food-and-ag-processing/gf2-growing-value-commercialization.html",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "22":{
    "DefaultCategory":"Grant",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/nbifnewbrunswickinnovationfoundationlogo.png",
      "alt":"New Brunswick Innovation Foundation Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Startup Investment Fund",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Grant",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"up to $100,000",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"New Brunswick",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"Ongoing",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"No",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Any",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"Incorporated, investment-ready startup in New Brunswick",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"Apply via website",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Any",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"https://nbif.ca/en/venture_capital/startup_investment_fund/",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "23":{
    "DefaultCategory":"Grant",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/governmentofnewbrunswicklogo.png",
      "alt":"Government of New Brunswick Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Strategic Initiatives Fund",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Grant",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"Varies",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"New Brunswick",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"Ongoing",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"No",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Any",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"Not-for-profit Arts Organizations and Cultural Associations; Cultural Industries,Municipalities with a cultural policy; First Nations groups",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"See website for application details",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Tourism, Arts & Entertainment, Recreation & Hospitality",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"http://www2.gnb.ca/content/gnb/en/services/services_renderer.201068.html#serviceDescription",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "24":{
    "DefaultCategory":"Grant",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/governmentofnewbrunswicklogo.png",
      "alt":"Government of New Brunswick Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Arts in Communities Program",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Grant",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"50% up to $10,000",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"New Brunswick",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"April  ",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"No",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Any",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":" Non-profit arts and cultural organizations legally incorporated in New Brunswick;  First Nations;  Municipalities with a cultural policy;  Multi-cultural associations",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"See website for application details",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Tourism, Arts & Entertainment, Recreation & Hospitality",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"http://www2.gnb.ca/content/gnb/en/departments/thc/services/services_renderer.201424.Arts_in_Communities_Program_(AIC).html",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "25":{
    "DefaultCategory":"Grant",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/governmentofnewfoundlandandlabradorlogo.png",
      "alt":"Government of Newfoundland and Labrador Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Business Investment Fund: Business Development Support Program",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Grant",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"50% up to $100,000",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"Newfoundland/Labrador",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"Ongoing",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"No",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Any",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"Be a small and medium-sized enterprise (SME) based and operating in Newfoundland and Labrador having fewer than 100 employees and less than $10 million in sales. Demonstrate its ability to undertake the work identified in the project proposal. Demonstrate its ability to access the financial capital required to complete the project. Operate in a strategic sector as defined by TCII. Be in good standing with the Government of Newfoundland and Labrador.",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"Forms on website",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Any",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"http://www.tcii.gov.nl.ca/programs/bif.html",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "26":{
    "DefaultCategory":"Grant",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/northwestterritoriesbusinessdevelopmentandinvestmentcorporationlogo.png",
      "alt":"Northwest Territories Business Development and Investment Corporation Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Business Development Project Fund",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Grant",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"maximum in any 5 year period is $10,000",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"Northwest Territories",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"Ongoing",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"No",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Any",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"Only businesses with $500,000 or less in annual revenues are eligible to apply",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":" Apply online",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Any",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"https://www.bdic.ca/financial-programs/contributions/",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "27":{
    "DefaultCategory":"Grant",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/governmentofnovascotialogo.jpg",
      "alt":"Government of Nova Scotia Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Energy Training Program for Students",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Grant",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"Employers receive up to 50% of the student’s wages for a 12 – 17 week work term.",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"Nova Scotia",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"Late February",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"No",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Any",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"Businesses in Nova Scotia with a direct connection to the energy sector. Co-op students must start work by May 21 and can work until August 24.",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"Visit the website and submit an application form.",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Mining, Energy Production, and Distribution",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"https://energy.novascotia.ca/industry-development/energy-training-program",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "28":{
    "DefaultCategory":"Grant",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/governmentofnovascotialogo.jpg",
      "alt":"Government of Nova Scotia Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Co-op Education Incentive",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Grant",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"Wage reimbursement for $7.50/hour up to a maximum of 40 hours/week.",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"Nova Scotia",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"Rolling deadlines based on fall, winter, spring terms",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"Yes",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Undergraduate",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"Private, non-profit, and government-funded organizations are eligible to apply. They must hire students enrolled in a Co-operative Education program at a Nova Scotia university.",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"Visit the website, fill out an application form, and submit it to the organization for approval.",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Any",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"https://novascotia.ca/programs/co-op-education-incentive/",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "29":{
    "DefaultCategory":"Grant",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/governmentofnovascotialogo.jpg",
      "alt":"Government of Nova Scotia Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Student Summer Skills Incentive Program",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Grant",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"Funding varies based on industry",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"Nova Scotia",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"Late January",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"No",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Undergraduate",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"Non-profit organizations that hire post-secondary students who are either residents of Nova Scotia or have resided in the province for at least 6 months before their work term. Students must work between April 23 and August 31 each year.",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"Visit the website, fill out the External Access Form, and email it to the organization for your account to be set up. After the account is set up, you can apply.",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Any",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"https://novascotia.ca/programs/student-summer-skills-incentive/",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "30":{
    "DefaultCategory":"Grant",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/governmentofnovascotialogo.jpg",
      "alt":"Government of Nova Scotia Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Workplace Innovation and Productivity Skills Incentive",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Grant",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"50-100% up to $10,000",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"Nova Scotia",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"Ongoing",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"No",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Any",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"Located in Nova Scotia, Registered and active in the Nova Scotia Registry of Joint Stock Companies or incorporated by an Act of the Nova Scotia Legislature, Over one calendar year into operations, Generating directly or representing businesses (e.g. Sector councils) that have at least thirty percent (30%) of revenues from commercial activity*, Producing a minimum of $25,000 in wages or owner remuneration, Not solely generating profit from wholesale, retail, and accommodations and foodservices (e.g. businesses that manufacture and sell products)?",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"Apply via website",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Any",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"https://novascotia.ca/programs/workplace-innovation-productivity-skills-incentive/",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "31":{
    "DefaultCategory":"Grant",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/governmentofnovascotialogo.jpg",
      "alt":"Government of Nova Scotia Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Graduate to Opportunity",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Grant",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"25% of the first year’s salary – 35% if the new grad is a member of designated diversity group – and 12.5% of the second year’s salary",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"Nova Scotia",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"Ongoing",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"No",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Alumni",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"Small businesses of fewer than 100 employees. Start-up companies incorporated within two years of the application date. Social enterprises, not-for-profit organizations and registered charities with recognized standing.",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"Apply via website",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Any",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"https://novascotia.ca/programs/graduate-to-opportunity/",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "32":{
    "DefaultCategory":"Grant",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/governmentofnunavutlogo.jpg",
      "alt":"Department of Economic Development and Transportation, Government of Nunavut Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Community Tourism and Cultural Industries Program",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Grant",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"Min. $1,000 up to $100,00 depending on the stream",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"Nunavut",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"April 15, August 15, December 15",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"No",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Any",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"A Nunavut small business includes any of the following: • Nunavut business, typically with less than $500,000 in annual gross sales or fewer than 10 employees; • Artist, craftsperson or performing artist who makes all or part of their income through the sale of products they produce or plan to produce; or • A Nunavut outfitter with a licence, with less than $500,000 in annual gross sales.",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":" More info on website",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Any",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"https://www.gov.nu.ca/edt/programs-services/community-tourism-and-cultural-industries-program",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "33":{
    "DefaultCategory":"Grant",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/governmentofnunavutlogo.jpg",
      "alt":"Department of Economic Development and Transportation, Government of Nunavut Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Small Business Support Program",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Grant",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"50-80%",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"Nunavut",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"Ongoing",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"No",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Any",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"Eligible applicants must have reported a minimum of $10,000 in gross business\r\nsales to Canada Customs and Revenue Agency in at least one of the last three\r\ntax years or be considered a new entrant.",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":" More info on website",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Any",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"https://www.gov.nu.ca/developpement-economique-et-des-transports/programs-services/small-business-support-program",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "34":{
    "DefaultCategory":"Grant",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/governmentofnunavutlogo.jpg",
      "alt":"Department of Economic Development and Transportation, Government of Nunavut Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Nunavut Business Investment Fund",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Grant",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"$25,000 up to $250,000",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"Nunavut",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"Ongoing",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"No",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Any",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"Corporation, registered and in good standing; Partnership or sole proprietorship duly registered and in good standing with Nunavut Legal Registries, and a holder of a valid Nunavut business license; or Co-operative association in good standing under the Co-operative Associations Act.",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"Contact info on website",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Any",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"https://www.gov.nu.ca/edt/programs-services/strategic-investments-program",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "35":{
    "DefaultCategory":"Grant",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/governmentofnunavutlogo.jpg",
      "alt":"Department of Economic Development and Transportation, Government of Nunavut Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Nunavut Economic Foundations Fund",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Grant",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"$50,000 up to $250,000",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"Nunavut",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"Ongoing",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"No",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Any",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"Applicants must be registered and in good standing under: Nunavut’s Societies Act; The Canada Not for Profit Corporations Act; or Nunavut’s Cities, Towns and Villages Act or the Hamlets Act.",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"Contact info on website",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Any",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"https://www.gov.nu.ca/edt/programs-services/strategic-investments-program",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "36":{
    "DefaultCategory":"Grant",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/feddevontariologo.jpg",
      "alt":"FedDev Ontario, Government of Ontario Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Investing in Business Growth and Productivity",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Grant",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"Stream 1: $20 mil per project up to 25% of eligible costs. Stream 2: $20 mil per project up to 100% of eligible costs.",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"Ontario",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"Ongoing",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"No",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Any",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"Small/medium sized enterprizes withat least 15 employees and sustianable business model, profitable track record and potential to be global player OR Not-for-profit",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"Apply online, 2-phase process for for-profit applicants, 1-phase for not-for-profit applicants and provide additional information for each",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Any",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"http://www.feddevontario.gc.ca/eic/site/723.nsf/eng/h_01867.html?OpenDocument",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "37":{
    "DefaultCategory":"Grant",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/feddevontariologo.jpg",
      "alt":"FedDev Ontario, Government of Ontario Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Investing in Business Innovation",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Grant",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"Stream 1: $20 million per project for up to 100% of eligible costs: $10,000 per new entrepreneur with matching funding for 50% up to $30,000. Stream 2: $1 mil per project up to 33.3%. Stream 3: $500,000 up to 100%.",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"Ontario",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"Ongoing",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"No",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Any",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"Not-for profits providing skills development and seed funding to new entrepreneurs, early-stage businesses with less than 50 employees, Southern Ontario-based angel investor networks",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"Apply online, single-phase application and provide additional information",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Any",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"http://www.feddevontario.gc.ca/eic/site/723.nsf/eng/h_00324.html?OpenDocument",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "38":{
    "DefaultCategory":"Grant",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/governmentofontariologo.jpg",
      "alt":"Government of Ontario Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Rural Economic Development program",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Grant",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"50% up to $100,000",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"Ontario",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"July-September",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"No",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Any",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"A legal entity that in not-for-profit, a municipality, a local services board, an Indigenous community or organization",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"Email application package with required documants (see website)",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Any",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"https://www.ontario.ca/page/rural-economic-development-program",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "39":{
    "DefaultCategory":"Grant",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/mentorworkslogo.png",
      "alt":"Mentor Works LTD Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Health Technologies Fund",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Grant",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"50% up to $100,000 or $500,000 depending on stream",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"Ontario",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"2017-06-21T04:00:00.000Z",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"No",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Any",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"Must contain all 3 research partner types: 1. One or more provider(s) of publicly-funded health services. 2. An Ontario-based health technology company. 3. Academic researchers.",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"Submit Problem Statement Intake Form, if approved then a more detailed application is required",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Health Care",
        "1":" Ergonomics, Sports & Fitness, Occupational Health",
        "2":"  Research",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"https://www.mentorworks.ca/blog/government-funding/health-technologies-fund-overview/",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "40":{
    "DefaultCategory":"Grant",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/yveslandryfoundationlogo.png",
      "alt":"Yves Landry Foundation Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Achieving Innovation and Manufacturing Excellence",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Grant",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"Up to $50,000",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"Ontario",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"Ongoing",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"No",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Any",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"Southern Ontario manufacturers undertaking training or skills-upgrading activities which will support innovation at the manufacturing facilities. All initiatives must support innovation which will lead to new global export opportunities or create new global markets.",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"Apply via their website",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Materials, Manufacturing, Processing and Systems Engineering",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"http://www.yveslandryfoundation.com/programs/aime-initiative/aime-global/aime-global-english/",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "41":{
    "DefaultCategory":"Grant",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/governmentofontariologo.jpg",
      "alt":"Government of Ontario Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Eastern Ontario Development Fund",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Grant",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"15 - 50% of eligible project costs up to 1.5 million",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"Ontario",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"Ongoing",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"No",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Any",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"Businesses that support economic development in Eastern Ontario. This includes job creation, innovation, and the ability to attract private sector investment.",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"Visit the website to contact a regional staff member who will determine eligibility and guide you through the application process.",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Any",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"http://www.grants.gov.on.ca/GrantsPortal/en/OntarioGrants/GrantOpportunities/PRDR017554",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "42":{
    "DefaultCategory":"Grant",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/ministryofagriculturefoodandruralaffairsgovernmentofontariologo.jpg",
      "alt":"Ministry of Agriculture, Food and Rural Affairs, Government of Ontario Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Export Market Access: A Global Expansion Program",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Grant",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"Up to 50% of eligible expenses. Eligible expenses are those incurred when undertaking activities in: direct contacts, marketing tools, market research, or foreign bidding projects.",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"Ontario",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"At least 30 days before the student’s start date",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"Yes",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Undergraduate",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"Operations at least two years old with 5 – 200 employees and a minimum annual sales totalling $500,000 who have hired co-op students to undertake one of the four activities mentioned above.",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"Application form on website.",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Any",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"http://www.omafra.gov.on.ca/english/food/industry/exp-mkt-access.htm",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "43":{
    "DefaultCategory":"Grant",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/governmentofontariologo.jpg",
      "alt":"Government of Ontario Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Jobs and Prosperity Fund",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Grant",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"Varies",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"Ontario",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"Ongoing",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"No",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Any",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"Businesses that need funding for projects that will enhance productivity and help them to compete in the global market place. There are four streams: New Economy, Food and Beverage, Strategic Partnerships, and Forestry Growth.",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"Visit the website to contact a staff member who will determine eligibility and guide you through the application process.",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Business Development, Sales, Marketing and Communications",
        "1":" Tourism, Arts & Entertainment, Recreation & Hospitality",
        "2":" Mining, Energy Production, and Distribution",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"http://www.grants.gov.on.ca/GrantsPortal/en/OntarioGrants/GrantOpportunities/PRDR017552",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "44":{
    "DefaultCategory":"Grant",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/northernontarioheritagefundcorporationlogo.png",
      "alt":"Northern Ontario Heritage Fund Corporation Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Norhtern Ontarion Heritage Fund Corporation",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Grant",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"50 - 75% of eligible expenses",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"Ontario",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"End of March",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"No",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Any",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"Businesses in Northern Ontario who fall under one of the programs available through the Northern Ontario Heritage Fund Corporation: Northern Business Opportunity Fund, Northern Innovation Program, Strategic Economic Infrastructure Program, Northern Community Capacity Building Program, Northern Event Partnership Program, or the Northern Ontario Internship Program.",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"Visit the Fund’s website for more detailed information and to fill out an application form.",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Any",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"https://nohfc.ca/en/programs",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "45":{
    "DefaultCategory":"Grant",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/ontariocentresofexcellencelogo.png",
      "alt":"Ontario Centres of Excellence Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Collaboration Voucher Program",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Grant",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"Varies depending on the program, but typically covers 50% of eligible research expenditures",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"Ontario",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"Ongoing",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"No",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Undergraduate",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"Businesses who want to hire students from Ontario post-secondary institutions to address industry challenges and improve productivity, performance, and competitiveness. There are three different voucher programs that business can apply to that are geared toward the development of innovative products, processes, and services.",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"Visit the website, browse program options, and fill out the appropriate application.",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Any",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"http://www.oce-ontario.org/programs/industry-academic-collaboration/collaboration-voucher-program",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "46":{
    "DefaultCategory":"Grant",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/smartprogramlogo.png",
      "alt":"SMART Program Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Smart Program: Advanced Technologies for Global Growth",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Grant",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"35% funding to a maximum of $100,000 per project",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"Ontario",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"Ongoing",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"No",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Any",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"Eligible small and medium-sized manufacturing businesses in Southern Ontario that are engaged in international markets (ie: companies that export, are going to export, or are selling in a value chain leading to export). The project must demonstrate a focus on adapting or adopting advanced technologies.",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"Visit the website and fill out an application form.",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Any",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"http://www.cme-smart.ca/feddev-projects-en",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "47":{
    "DefaultCategory":"Grant",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/smartprogramlogo.png",
      "alt":"SMART Program Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Smart Program: Operation Assessment Funding for Northern Ontario Manufacturers",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Grant",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"50% of eligible costs, up to $15,000",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"Ontario",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"Ongoing",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"No",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Any",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"Manufacturers based in Northern Ontario with fewer than 500 employees who are interested in receiving operational assessments in order to expand their business. These assessments can include: productivity, waste reduction, lean manufacturing, management systems, market analysis, process flow, and quality certification.",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"Contact a CME Qualified Service Provider and inform them of your business’ project.",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Materials, Manufacturing, Processing and Systems Engineering",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"http://www.cme-smart.ca/fednor-qsp-list-en",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "48":{
    "DefaultCategory":"Grant",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/governmentofontariologo.jpg",
      "alt":"Government of Ontario Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Southwestern Ontario Development Fund",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Grant",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"15 - 50% of eligible project costs up to 1.5 million",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"Ontario",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"Ongoing",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"No",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Any",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"Businesses that support economic development in Southwestern Ontario. This includes job creation, innovation, and the ability to attract private sector investment.",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"Contact a regional staff member who will determine eligibility and guide you through the application process.",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Any",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"http://www.grants.gov.on.ca/GrantsPortal/en/OntarioGrants/GrantOpportunities/PRDR017556",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "49":{
    "DefaultCategory":"Grant",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/universityofwaterloologo.png",
      "alt":"University of Waterloo Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Undergraduate Research Internship",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Grant",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"Up to $1600/term",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"Ontario",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"Ongoing",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"No",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Undergraduate",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"UWaterloo faculty members hiring an undergraduate student to work on research under their supervision.",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"Visit the website and fill out an application form.",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Research",
        "1":" Teaching and Educational Services",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"https://uwaterloo.ca/co-operative-education/undergraduate-research-internship-uri-program",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "50":{
    "DefaultCategory":"Grant",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/governmentofprinceedwardislandlogo.jpg",
      "alt":"Government of Prince Edward Island  Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Small Business Investment Grant",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Grant",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"15% up to $3,750",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"PEI",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"Ongoing",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"No",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Any",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"Eligible small businesses operating on Prince Edward Island.",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"Apply via website",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Any",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"https://www.princeedwardisland.ca/en/service/apply-small-business-investment-grant",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "51":{
    "DefaultCategory":"Grant",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/governmentofprinceedwardislandlogo.jpg",
      "alt":"Government of Prince Edward Island  Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Marketing Assistance",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Grant",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"25-40% up to $40,000",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"PEI",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"Ongoing",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"No",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Any",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"A company (sole proprietorship, partnership, cooperative or limited liability) is eligible for funding if it has a well-developed business and marketing plan and manufactures, processes, or provides exportable services in one of the following sectors: food development, diversified manufacturing, information technology, bioscience, craft/giftware. First-of-its-kind exportable services in other sectors may be considered. Retail and consumer services businesses are not eligible for assistance with the Marketing Support Assistance. The project must provide economic benefit to the province and cannot jeopardize similar businesses.",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"Apply online",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Materials, Manufacturing, Processing and Systems Engineering",
        "1":" Computing: Hardware & Software, Information Systems, and Security",
        "2":" AI, Robotics and Data Science",
        "3":" Tourism, Arts & Entertainment, Recreation & Hospitality",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"https://www.princeedwardisland.ca/en/service/apply-marketing-assistance",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "52":{
    "DefaultCategory":"Grant",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/governmentofsaskatchewanlogo.jpg",
      "alt":"Government of Saskatchewan Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Saskatchewan Job Grant",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Grant",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"multiple grant applications to a maximum of $100,000 per year and up to $10,000 per individual",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"Saskatchewan",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"Ongoing",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"No",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Alumni",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"Private and not-for-profit sector employers are eligible.",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"Visit the website and fill out the applicable forms",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Any",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"https://www.saskatchewan.ca/business/hire-train-and-manage-employees/apply-for-the-canada-saskatchewan-job-grant",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "53":{
    "DefaultCategory":"Grant",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/yukonartscenterlogo.png",
      "alt":"Yukon Arts Center Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Cultural Industries Training Fund",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Grant",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"Varies based on project, funding preference given to projects that require less than $5,000 for individual training and less than $8,000 for group training, 90% of funds will be released upon approval and submission of invoice, and 10% upon submission of a completed final report",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"Yukon",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"January 15, March 15, May 15, September 15",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"No",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Any",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"All persons resident in the Yukon for a minimum of one year shall be eligible for support from the CITF. Commercial and non-for-profit organizations may apply.",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"Apply online",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Tourism, Arts & Entertainment, Recreation & Hospitality",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"http://yukonartscentre.com/programs/citf",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "54":{
    "DefaultCategory":"Grant",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/governmentofyukonlogo.gif",
      "alt":"Government of Yukon Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Product development partnership program",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Grant",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"$25,000/year for one, or multiple projects",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"Yukon",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"Ongoing",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"No",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Any",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"Your project must start and end in the same fiscal year (April 1 to March 31), You must be able to provide 10% equity (5% cash, 5% in-kind). Other Yukon government funding may not be used as equity. Your organization must be in good standing with Yukon Corporate Affairs",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"Visit website for contact information",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Tourism, Arts & Entertainment, Recreation & Hospitality",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"http://www.tc.gov.yk.ca/pdpp.html",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "55":{
    "DefaultCategory":"Grant",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/yukonmediadevelopmentlogo.jpg",
      "alt":"Yukon Media Development Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Yukon Film Development Fund",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Grant",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"Up to 50% of the actual Yukon expenditures to a maximum of $35,000 or 33% of total project expenses",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"Yukon",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"Ongoing",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"No",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Any",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"An individual who is a Yukon resident and who owns at least 51% of the project or a Yukon corporation which owns at least 51% of the project and film production is a major, not peripheral activity of their business.",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"Visit website for application materials",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Tourism, Arts & Entertainment, Recreation & Hospitality",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"https://www.reelyukon.com/FundingPrograms/yukonfilmmakers/developmentfund",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "59":{
    "DefaultCategory":"Subsidy",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/governmentofalbertalogo.png",
      "alt":"Government of Alberta Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Summer Temporary Employment Program (STEP)",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Subsidy",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"Wage subsidy of $7/hour up to 30 hours per week",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"Alberta",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"Early February",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"No",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Undergraduate",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"Small businesses in Alberta (under 50 employees) hiring post-secondary students who reside and/or attend school in Alberta. The position must be full-time, 4 – 16 weeks long, and fall between May 1st and August 31st.",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"Visit the website and fill out an application form.",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Any",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"http://www.albertacanada.com/employers/recruit/step-application-process.aspx",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "60":{
    "DefaultCategory":"Subsidy",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/biotalentcanadalogo.png",
      "alt":"BioTalent Canada Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Career Focus Green Jobs",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Subsidy",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"Up to $7000 in co-op wage subsidies",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"Canada",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"Varies depending on the fund program.",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"Yes",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Any",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"Businesses in the Biotech sector that have a green mandate and are hiring a co-op student for a role with an environmental focus. This organization offers funding opportunities for both co-op students and new graduates.",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"Visit the website and submit an application form to the fund of your choice.",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Geomatics & GIS",
        "1":" Environmental and Natural Resource Management, and Sustainability",
        "2":" Mining, Energy Production, and Distribution",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"https://www.biotalent.ca/en/wage-subsidies?utm_source=iContact&utm_medium=email&utm_campaign=BioTalent%2520Canada%2520&utm_content=HR+Microscope+September+2016#SWILP",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "61":{
    "DefaultCategory":"Subsidy",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/ecocanadalogo.png",
      "alt":"Eco Canada Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Environmental Youth Corporation Internship Program",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Subsidy",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"Up to 50% of an employee’s wage up to $15,000",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"Canada",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"None. Once you’ve filled out the application form, you’ll be notified within 10 days of your eligibility to participate. Your candidate must start within 30 days of being approved for the program.",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"No",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Any",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"Eligible employers who work in science, technology, engineering, mathematics, or natural resources. Employers must be hiring a new, full-time employee (under age 30) who has graduated from a post-secondary institution and will be working for for 8-12 months.",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"Visit the website and fill out an application form.",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Materials, Manufacturing, Processing and Systems Engineering",
        "1":" Computing: Hardware & Software, Information Systems, and Security",
        "2":" AI, Robotics and Data Science",
        "3":" Geomatics & GIS",
        "4":" Environmental and Natural Resource Management, and Sustainability",
        "5":" Mining, Energy Production, and Distribution",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"http://www.eco.ca/wage-subsidy-programs/im-an-employer/",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "62":{
    "DefaultCategory":"Subsidy",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/ecocanadalogo.png",
      "alt":"Eco Canada Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Graduate Enterprise Internship",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Subsidy",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"Up to 50% of a new employees wage",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"Canada",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"Ongoing",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"No",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Alumni",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"Employers who work in science, technology, engineering, mathematics, or natural resources are are looking to hire new full-time environmental jobs. This program may be used to fund multiple new positions.",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"Visit the website and submit an application form.",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Materials, Manufacturing, Processing and Systems Engineering",
        "1":" Computing: Hardware & Software, Information Systems, and Security",
        "2":" AI, Robotics and Data Science",
        "3":" Geomatics & GIS",
        "4":" Environmental and Natural Resource Management, and Sustainability",
        "5":" Mining, Energy Production, and Distribution",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"http://www.eco.ca/wage-subsidy-programs/",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "63":{
    "DefaultCategory":"Subsidy",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/governmentofmanitobalogo.png",
      "alt":"Manitoba Provincial Government Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"STEP Manitoba",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Subsidy",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"Varies",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"Manitoba",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"December",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"No",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Undergraduate",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"Government departments",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"Gvoernment employees request via the STEP Services",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Any",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"http://www.gov.mb.ca/cyo/studentjobs/",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "64":{
    "DefaultCategory":"Subsidy",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/nbjobslogo.png",
      "alt":"NBJobs Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Student Employment Experience Development (SEED)",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Subsidy",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"50% (Private sector organizations may be eligible for a 50% wage subsidity for up to 14 weeks. Non-profit organizations may be eligible for 100% wage subsidy.",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"New Brunswick",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"Ongoing",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"No",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Undergraduate",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"Employers in New Brunswick who are hiring students enrolled full-time in a post-secondary institution. Students must be residents of New Brunswick and work for a maximum of 14 weeks between April 30 and the Friday after Labour Day each year.",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"Visit the website and fill out an application.",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Any",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"https://www.nbjobs.ca/seed/#program_employer",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "65":{
    "DefaultCategory":"Subsidy",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/governmentofnewfoundlandandlabradorlogo.png",
      "alt":"Government of Newfoundland and Labrador Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Student Summer Employment Program (Post-Secondary)",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Subsidy",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"Private sector organization may be eligible for a wage subsidy of $5.60/hour. Non-for-profit organizations may be eligible for a subsidy of 100% of minimum wage plus 15% to offset employment related costs",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"Newfoundland/Labrador",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"Early May",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"No",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Undergraduate",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"Both non-for-profit and private sector employers with an established operation in Newfoundland and Labrador. Employers must hire students for 5- 12 weeks and they must work from 25 – 40 hours per week.",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"Visit the website and fill out an application form.",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Any",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"http://www.aesl.gov.nl.ca/students/ssep_post_secondary.html",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "66":{
    "DefaultCategory":"Subsidy",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/governmentofyukonlogo.gif",
      "alt":"Government of Yukon Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Yukon STEP Program",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Subsidy",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"Wage subsidy of $7.20/hour for 450 to 600 hours",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"Yukon",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"Early November",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"No",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Undergraduate",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"Yukon employers who hire Yukon post-secondary students for summer jobs in career-related fields of study.",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"Visit the website and fill out an application form.",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Any",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"http://www.education.gov.yk.ca/step-employer-info.html",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  },
  "92":{
    "DefaultCategory":"Subsidy",
    "Image":{
      "src":"https://uwaterloo.ca/hire/sites/ca.hire/files/uploads/images/projectlearningtreecanadalogo.png",
      "alt":"Project Learning Tree Canada Logo",
      "filter":false,
      "hidden":false
    },
    "Name":{
      "text":"Project Learning Tree Canada",
      "filter":false,
      "hidden":false
    },
    "Category":{
      "Type":{
        "0":"Subsidy",
        "filter":true,
        "hidden":true
      },
      "Amount":{
        "0":"50% wage-matching for qualified jobs",
        "filter":false,
        "hidden":false
      },
      "Location":{
        "0":"Canada",
        "filter":true,
        "hidden":false
      },
      "Deadline":{
        "0":"2018-12-31T05:00:00.000Z",
        "filter":false,
        "hidden":true
      },
      "Co-op Specific":{
        "0":"Yes",
        "filter":true,
        "hidden":true
      },
      "Student Type":{
        "0":"Undergraduate",
        "filter":true,
        "hidden":true
      },
      "Who Can Apply":{
        "0":"Any organization that will offer work experience opportunities to students in the green jobs sector. Work experiences must be from 8-16 weeks in duration and must take place within Canada. The job must be a Green Job from the Approved Jobs List: http://pltcanada.org/wp-system/uploads/2018/07/approved-jobs-list-sfi-network.pdf",
        "filter":false,
        "hidden":true
      },
      "How To Apply":{
        "0":"Contact Project Learning Tree Canada to get started",
        "filter":false,
        "hidden":true
      },
      "Industry":{
        "0":"Research",
        "1":" Tourism, Arts & Entertainment, Recreation & Hospitality",
        "2":" Architecture & Design, Construction, Infrastructure, Urban Planning",
        "3":" Geomatics & GIS",
        "4":" Environmental and Natural Resource Management, and Sustainability",
        "filter":true,
        "hidden":true
      }
    },
    "Link_URL":{
      "bold":"Click Here to Learn More",
      "href":"http://pltcanada.org/index.php/news-releases/fall-funding-available-for-employers-placing-students-in-green-jobs/",
      "alttext":"Visit Here for More Info",
      "target":"_blank",
      "filter":false,
      "hidden":true
    }
  }
}';

$json = json_decode($json,true);
/**
* @param $json
* @return string
*/
function json_to_html(&$json_array) {
  $catProps = array();
  $default = [];

  $html_out = "<div id=\"filter-start\">\n";

  //Categorizing into default category
  foreach ($json_array as $key => $value) {
    $default[$value['DefaultCategory']][$key] = $value;
  }

  //sorting
  ksort($default);//Sorts default categories in ascending order
  foreach ($default as $value) {
    ksort($value);//sorts products in ascending order
  }

  //Categorizing into filter categories
  foreach ($json_array as $jsonArr => $arrTitle) {
    foreach($arrTitle as $indexParam => $indexTitle){
      $bool = false;
      if($indexParam == "DefaultCategory"){
        continue;
      } elseif($indexParam == "Category") {
        foreach($indexTitle as $paramTitle => $paramValues){
          //searches for category title
          foreach($paramValues as $filter => $boolean){
            if($filter === "filter" && $boolean) {
              $bool=true;
            }
          }
          //inserts category title in array if not in array
          if($bool == true && !array_key_exists($paramTitle, $catProps)) {
            $catProps[$paramTitle] = array();
          }
          //inserts category option in array if not in array already
          foreach($paramValues as $filter => $boolean){
            $newParamValues = explode(", ", $paramValues[0]);
            if(array_key_exists($paramTitle, $catProps)) {
              if(is_string($newParamValues)){ //checks and inserts for a single parameter
                if($filter === 'filter' && $boolean === true && !in_array($paramValues[0], $catProps[$paramTitle])){
                  array_push($catProps[$paramTitle], $paramValues[0]);
                }
              } else { //breaks up string with multiple aprameters and inserts if not already in filter array
                foreach($newParamValues as $string) {
                  $newStr = str_replace('and ', '', $string);
                  if($filter === 'filter' && $boolean === true && !in_array($newStr, $catProps[$paramTitle])){
                    array_push($catProps[$paramTitle], $newStr);
                  }
                }
              }
            }
          }
          //reset bool
          $bool = false;
        }
      } else {
        continue;
      }
    }
  }

  //Sorting $catProps
  foreach($catProps as $key => $value) {
    sort($catProps[$key]);
  }

  //Filter Parameters
  $html_out .= "<div id='filter'>\n";

  //Filter Buttons
  $html_out .= "<p id='filterBy-text'>Filter By...</p>\n";
  foreach ($catProps as $select => $param) {
    $html_out .= "<select>\n<option value='{$select}' disabled selected>{$select}</option>\n";
    foreach($param as $option) {
      $html_out .= "<option value='{$option}'>{$option}</option>\n";
    }
    $html_out .= "</select>\n";
  }

  //Search Box
  $html_out .= "<div id='filter'>\n";
  $html_out .= "<div class='button_box2'>\n<div class='form-wrapper-2 cf'>\n";
  $html_out .= "<input id='search' type='text' placeholder='Search Here...' required>\n";
  $html_out .= "<button id='searchBtn'>Search</button>\n</div>\n</div>\n";

  //Clear button
  $html_out .= "<button id='clear'>Clear</button>\n</div>\n";

  $html_out .= "</div>\n";

  //Converting JSON to html elements
  foreach ($default as $defaultcategory => $value) {//each default category
    if(!preg_match('/^</', $defaultcategory)){
      $html_out .= "<div class=\"filter-defaultcategory\">\n<h2>{$defaultcategory}</h2>\n";
      $html_out .= "<div id='masonry'>\n";
      foreach ($value as $uniqueid => $product) {//each product
        $html_out .= "<div class=\"product-vert filter-product-{$uniqueid}\">\n";
        $html_out .= "<div class=\"text \">\n";
        foreach ($product as $key => $field) {//each field
          $html_out .= array_to_html($key, $field, true); //outputs html
        }
        $html_out .= "</div>\n\n";

        //Button
        $html_out .= "<button class='button-vert modalBtn'>Learn More</button>\n";
        //Pop-up Window (modal)
        $html_out .= "<div class='modal-window'>\n";
        $html_out .= "<div class='modal-content'>\n<span class='close'>&times;</span>\n";
        $html_out .= "<h2>{$product['Name']['text']}</h2>\n";
        $html_out .= "<div class='contentDiv'>\n";
        //Modal Content
        foreach ($product as $key => $field) {//each field
          $html_out .= array_to_html($key, $field, false); //outputs html
        }
        $html_out .= "</div>\n</div>\n</div>\n</div>\n";
      }

      $html_out .= "</div>\n<hr>\n</div>\n\n";
    }
  }
  $html_out .= "</div>\n";

  return $html_out;
}

/**
* @param $input_key
* @param $input_array
* @return string
*/
function array_to_html($input_key, $input_array, $input_hide)
{
  $output = '';
  switch (explode("_", $input_key)[0]) {
    case "DefaultCategory":
    break;
    case "Name":
    if($input_hide == true){
      $output .= "<h4>{$input_array['text']}</h4>\n";
    } else {
      continue;
    }
    break;
    case "Link": //Case for link to make button in modal
    if($input_hide == false) {
      if(strpos($input_array['href'], '@')){
        $output .= "<a href=\"mailto:{$input_array['href']}\" target= \"{$input_array['target']}\" class='button'>{$input_array['bold']}</a>\n";
      } else if ($input_array['href']) {
        $output .= "<a href=\"{$input_array['href']}\" target= \"{$input_array['target']}\" class='button'>{$input_array['bold']}</a>\n";
      }
    }
    break;
    case "Image": //Case for photo on thumbnail
    if($input_array['src']){
      if ($input_hide == true) {
        $output .= "<img class='vertImg' src=\"{$input_array['src']}\" alt=\"{$input_array['alt']}\">\n";
      } else { //Case for major display in modal
        $output .= "<img class='horImg' src=\"{$input_array['src']}\" alt=\"{$input_array['alt']}\">\n";
      }
    }
    break;
    case "Category":
    if($input_hide) { //if outputting to the thumbnail (therefore hiding most input)
      //checks to see if option is hidden
      foreach($input_array as $option => $data){
        $hide = true;
        foreach($data as $param => $value){
          if($param === "hidden" && $value === false){
            $hide = false;
          }
        }
        //if it is shown on thumbnail, output
        if($hide === false){
          $output .= "<p><strong>{$option}: </strong>{$data[0]}</p>\n";
        }
      }
    } else { //otherwise outputting all input into table in modal
      $output .= "<table class='lowerTable'>\n";
      foreach($input_array as $option => $data){
        $output .= "<tr>\n<td class='title'><strong>{$option}</strong></td>\n";
        $output .= "<td class='info'>{$data[0]}</td>\n</tr>\n";
      }
      $output .= "</table>\n";
    }
    break;
  }
  return $output;
}
print_r(json_to_html($json));
