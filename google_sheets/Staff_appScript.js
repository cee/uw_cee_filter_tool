/**
 * @file
 * @author Jaskaran Ghotra
 * @author Gerry Saporito gksapori@edu.uwaterloo.ca
 */

/**
 * @description function main links to 'Funding Opps' sheet in Google Sheets program by URL
 * Selects all data in cells then loops through rows and columns of data
 * If a certain tyoe of funding is selected, pass by reference attributes function
 * @param {void}
 * @returns {array}
 * function returns arrays of rows filled with column values
 */
function main() {
    var data = SpreadsheetApp.openByUrl('https://docs.google.com/spreadsheets/d/1sqDHBzt5gMstBzYI_ysNlG3Yg8H3ioWjOyEydHfV-LA/edit#gid=1088817830');
    var sheet = data.getSheetByName('Staff');

    var range = sheet.getDataRange(); //Selects all data in sheet 'Staff'
    var values = range.getValues(); //Assigns all values within range to values

    var returnData = {};
    for (i = 1; i < values.length; i++) { //Loop through every row, parsing from left to right
      var product = {};
      attributes(i, product, values); //Pass by reference the attributes function to input productobj for every row
      returnData[i] = product; //Assign product values to every row in 'Staff' sheet
    }
    return returnData;
};


/** Contains switch case to append to object
 * @param {int} Input curent row
 * @param {int} Input current column
 * @param {obj} Input product object
 * @param {int} Input values from sheet 'Funding Opps'
 * @description appends fundobj with required column information form 'Funding Opps' sheet
 * @return {void}
 */
function attributes(rows, fundobj, values) {
    if (fundobj["DefaultCategory"] === undefined) {
        fundobj["DefaultCategory"] = values[rows][6];
    }
    fundobj["Image"] = {//Case for Images
        "src": values[rows][11],
        "alt" : "Photo of " + values[rows][1] + " " + values[rows][0],
        "filter": false,
        "hidden": false
    }

    fundobj["Name"] = {//Case for name
        "text": values[rows][1]+ " " + values[rows][0],
        "filter": false,
        "hidden": false
    }

    fundobj["Category"] = {//Cases to be used in filtering
        "Location": { //Case for Location
             "0": values[rows][2],
             "filter": false,
             "hidden": true,
        },
        "Extension": { //Case for phone number extension
            "0": values[rows][3],
            "filter": false,
            "hidden": true
        },
        "Job Title": { //Case for Job Title
            "0": values[rows][5],
            "filter": true,
            "hidden": false
        },
        "Department": { //Case for department
            "0": values[rows][6],
            "filter": true,
            "hidden": false
        },
        "Unit": { //Case for unit
            "0": values[rows][7],
            "filter": true,
            "hidden": true
        },
        "Manager": { //Case for manager
            "0": values[rows][8],
            "filter": false,
            "hidden": true
        },
        "Employment Status": { //Case for employment status
            "0": values[rows][9],
            "filter": true,
            "hidden": true
        },
        "Group Email": {} //Create object before runnning indLoop function
    };
    indLoop(rows, values, fundobj); //pass by reference from function below

    fundobj["Link_URL"] = {//Case for URL
        "bold": "Email for More Info",
        "href": "mailto:" + values[rows][4],
        "alttext": "Email for More Info",
        "target": "",
        "filter": false,
        "hidden": true
    }
}
/**
 * @description select cells in industry columns
 * Splits cell values into array seperated by semi-colon
 * function loops through cells in Industry column
 * if index is of ; skip through, else add in an object key, value pair to fundobj object
 * @param {int} input current row
 * @param {int} input current value
 * @return {void}
 */
function indLoop(rows, values, fundobj) {
    var ind_col = values[rows][10];
    var array_ind = ind_col.split(";#");
    for (var indval in array_ind) { //for every value in array_ind add key:value pair
        //assigns numbered key from array to industry values
        fundobj["Category"]["Group Email"][indval] = array_ind[indval];
    };
    fundobj["Category"]["Group Email"]["filter"] = true;
    fundobj["Category"]["Group Email"]["hidden"] = true;
}
/**
 * @param {*}
 * @description
 * Hooks into App Script getMethod() [Allows for API Call]
 * @returns JSON object
 */
function doGet(e) {
    var output = main();
    return ContentService.createTextOutput(JSON.stringify(output)).setMimeType(ContentService.MimeType.JSON);
}
