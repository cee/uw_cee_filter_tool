# JSON output format guide
A procedural method is taken to output data into JSON format.
The JSON file should begin with a unique product ID.
Each product ID has info fields (shown below).
All product page elements will be printed based on JSON order.
DO NOT rename main JSON keys, i.e. Category, Link, Photo, etc.

>The hidden attribute will prevent the information being displayed during overview but will still be displayed in product view.
>In addition to the hidden attribute, the filter attribute allows for the parameter to be considered as a filter category, and the information as a filter option.

#### DefaultCategory
**The default category that the product will be placed under.**
```javascript
"DefaultCategory": "Category",
```

#### Image
**Used to link photos.**
```javascript
"Image": {
    "src": "www.example.com/example.png",  
    "alt": "Alternate text that appears",
    "filter": boolean,
    "hidden": boolean
}
```

#### Name
**Used to distinguish between products**
```javascript
"name": {
    "text" : "Some Name",
    "filter": boolean,
    "hidden": boolean
}
```

#### Category
**Used to filter products.**
**Use numbers for category attribute keys**
**NOTE: This section is for all other information (general) that you want to be displayed but does not follow the format of the above/below objects**
```javascript
"Category": {

  "Category_Name_0": {
    "0": "Some text/info",
    "filter": boolean,
    "hidden": boolean
  },
  "Category_Name_1": {
      "0": "Some text/info",
      "filter": boolean,
      "hidden": boolean
   },
   "Category_Name_2": {
       "0": "Some text/info",
       "filter": boolean,
       "hidden": boolean
     },
}
```

#### Link_{Unique_ID}
**Used to add link elements to buttons.**
```javascript
"Link_{Unique_ID}": {
      "bold": "Text to be displayed as button",
      "href": "https://www.example.com",
      "alttext": "Alternate text that appears",
      "target": "open in new window etc. (eg. '_blank' opens in a new window",
      "filter": boolean,
      "hidden": boolean
      }
```
1. For emails, add {mailto:} before the email url in href
2. For target, consult https://www.w3schools.com/tags/att_link_target.asp
