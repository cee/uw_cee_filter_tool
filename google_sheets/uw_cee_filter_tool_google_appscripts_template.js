/**
 * @file
 * @author Gerry Saporito; gksapori@edu.uwaterloo.ca
 * Developer Notes: Google sheet must not include any apostrophes (') and "<...>" strings
 */

/**
 * @description function main links to sheet in Google Sheets program by URL (must change URL and Sheet Name)
 * Selects all data in cells then loops through rows and columns of data
 * @param {void}
 * @returns {array}
 * function returns arrays of rows filled with column values
 */
function main() {
    var data = SpreadsheetApp.openByUrl('SpreadSheet URL'); //---------------------------------------------------------------- CHANGE SpreadSheet URL
    var sheet = data.getSheetByName('Sheet Name'); //------------------------------------------------------------------------- CHANGE Sheet Name

    var range = sheet.getDataRange(); //Selects all data in sheet 'Staff'
    var values = range.getValues(); //Assigns all values within range to values

    var returnData = {};
    for (i = 1; i < values.length; i++) { //Loop through every row, parsing from left to right NOTE: i must start at 1, otherwise it will include the first row of titles which is unnecessary
      var product = {};
      attributes(i, product, values); //Pass by reference the attributes function to input productobj for every row
      returnData[i] = product; //Assign product values to every row in 'Staff' sheet
    }
    return returnData;
};


/** Contains switch case to append to object
 * @index "filter"- if you want this info to be a filter parameter, set to true, otherwise false
 * @index "hidden"- if you want this info to be hidden on the small , set to true, otherwise false
 * @param {obj} Input product object
 * @param {int} Input values from sheet
 * @description appends fundobj with required column information form Google sheet
 * @return {void}
 */
function attributes(rows, fundobj, values) {
    if (fundobj["DefaultCategory"] === undefined) {
        fundobj["DefaultCategory"] = values[rows][6];
    }
    fundobj["Image"] = {//Case for Images
        "src": values[rows]['column'],
        "alt" : "Photo of " + values[rows]['column'] + " " + values[rows]['column'],
        "filter": boolean,
        "hidden": boolean
    }

    fundobj["Name"] = {//Case for name
        "text": values[rows]['column']+ " " + values[rows]['column'], //or values[rows]['column']+ " " + values[rows]['column'] if first and last names are in seperate columns
        "filter": boolean,
        "hidden": boolean
    }

    fundobj["Category"] = { //Cases for general information to be used in filtering
        "Category Name to be displayed": { //Case for Category Name
             "0": values[rows]['column'],
             "filter": boolean,
             "hidden": boolean,
        },
        "Category Name to be displayed": { //Case for Category Name
            "0": values[rows]['column'],
            "filter": boolean,
            "hidden": boolean
        },
        "Category Name to be displayed": { //Case for Category Name
            "0": values[rows]['column'],
            "filter": boolean,
            "hidden": boolean
        },
        "Category Name for Category with multiple parameters (To be displayed)": {} //Create object before runnning indLoop function; can be done for multiple Categories; DELETE if not needed
    };
    indLoop(rows, values, fundobj); //pass by reference from function below; DELETE if "Category Name for Category with multiple parameters (To be displayed)" was deleted

    fundobj["Link_URL"] = { //Case for URL
        "bold": "Text In Button",
        "href": values[rows]['column'], //add  "mailto:" + before  values[rows]['column'] if link are emails
        "alttext": "Alternate Text In Button",
        "target": "", //Set "target": "_blank" if you want to open a new tab when button is clicked
        "filter": boolean,
        "hidden": boolean
    }
}
/**
 * @description select cells in "Category Name for Category with multiple parameters (To be displayed)" column
 * Splits cell values into array seperated by seperation character(s)
 * function loops through cells in "Category Name for Category with multiple parameters (To be displayed)" column
 * if index is of ; skip through, else add in an object key, value pair to fundobj object
 * @param {int} input current row
 * @param {int} input current value
 * @index "filter"- if you want this info to be a filter parameter, set to true, otherwise false
 * @index "hidden"- if you want this info to be hidden on the small , set to true, otherwise false
 * @return {void}
 */
function indLoop(rows, values, fundobj) {
    var ind_col = values[rows][10];
    var array_ind = ind_col.split(";#"); //MAY HAVE TO CHANGE, DEPENDING ON WHAT THE SEPERATION CHARACTER(s) IS/ARE
    for (var indval in array_ind) { //for every value in array_ind add key:value pair
        //assigns numbered key from array to industry values
        fundobj["Category"]["Category Name for Category with multiple parameters (To be displayed)"][indval] = array_ind[indval];
    };
    fundobj["Category"]["Category Name for Category with multiple parameters (To be displayed)"]["filter"] = boolean;
    fundobj["Category"]["Category Name for Category with multiple parameters (To be displayed)"]["hidden"] = boolean;
}
/**
 * @param {*}
 * @description
 * Hooks into App Script getMethod() [Allows for API Call]
 * @returns JSON object
 */
function doGet(e) {
    var output = main();
    return ContentService.createTextOutput(JSON.stringify(output)).setMimeType(ContentService.MimeType.JSON);
}
