/**
 * @file
 * @author Jaskaran Ghotra
 * @author Gerry Saporito
 */

/**
 * @description function main links to 'Funding Opps' sheet in Google Sheets program by URL
 * Selects all data in cells then loops through rows and columns of data
 * If a certain tyoe of funding is selected, pass by reference attributes function
 * @param {void}
 * @returns {array}
 * function returns arrays of rows filled with column values
 */
function main() {
    var data = SpreadsheetApp.openByUrl('https://docs.google.com/spreadsheets/d/1oHJZpxEWW4Ap9LgRS3XJWR3VmZoJTOmysQIc_6aorQU/edit#gid=585205525');
    var sheet = data.getSheetByName('Funding Opportunities - HIRE Waterloo');

    var range = sheet.getDataRange();
    var values = range.getValues();

    var returnData = {};
    for (var i = 0; i < values.length; i++) {
        var fund = {};
        if (values[i][2] === "Grant" || values[i][2] === "Subsidy" || values[i][2] === "Award" || values[i][2] === "Seed Funding" ) {
            attributes(i, fund, values);
            returnData[i] = fund;
        }
    };
    return returnData;
};


/** Contains switch case to append to object
 * @param {int} Input curent row
 * @param {int} Input current column
 * @param {obj} Input product object
 * @param {int} Input values from sheet 'Funding Opps'
 * @description appends fundobj with required column information form 'Funding Opps' sheet
 * @return {void}
 */
function attributes(rows, fundobj, values) {
    if (fundobj["DefaultCategory"] === undefined) {
        fundobj["DefaultCategory"] = values[rows][2];
    }
    fundobj["Image"] = {//Case for Images
        "src": values[rows][14],
        "alt" : values[rows][15] + " Logo",
        "filter": false,
        "hidden": false
    }

    fundobj["Name"] = {//Case for name
        "text": values[rows][0],
        "filter": false,
        "hidden": false
    }

    fundobj["Category"] = {//Cases to be used in filtering
        "Type": {
            "0": values[rows][2], //Case for type of funding
            "filter": true,
            "hidden": true
        },
        "Amount": { //Case for amount of money
            "0": values[rows][4],
            "filter": false,
            "hidden": false
        },
        "Location": { //Case for location
            "0": values[rows][3],
            "filter": true,
            "hidden": false

        },
        "Deadline": {
            "0": values[rows][8], //Case for deadline
            "filter": false,
            "hidden": true
        },
        "Co-op Specific": {
            "0": values[rows][9], //Case for co-op or non co-op
            "filter": true,
            "hidden": true
        },
        "Student Type": {
            "0": values[rows][10], //Case for undergraduate, graduate, or other
            "filter": true,
            "hidden": true
        },
        "Who Can Apply": { //Case for who can apply
            "0": values[rows][5],
            "filter": false,
            "hidden": true
        },
        "How To Apply": { //Case for how to apply
            "0": values[rows][7],
            "filter": false,
            "hidden": true
        },
        "Industry": {} //Create object before runnning indLoop function
    };
    indLoop(rows, values, fundobj); //pass by reference from function below

    fundobj["Link_URL"] = {//Case for URL
        "bold": "Click Here to Learn More",
        "href": values[rows][1],
        "alttext": "Visit Here for More Info",
        "target": "_blank",
        "filter": false,
        "hidden": true
    }
}
/**
 * @description select cells in industry columns
 * Splits cell values into array seperated by semi-colon
 * function loops through cells in Industry column
 * if index is of ; skip through, else add in an object key, value pair to fundobj object
 * @param {int} input current row
 * @param {int} input current value
 * @return {void}
 */
function indLoop(rows, values, fundobj) {
    var ind_col = values[rows][11];
    var array_ind = ind_col.split(";");
    for (var indval in array_ind) { //for every value in array_ind add key:value pair
        //assigns numbered key from array to industry values
        fundobj["Category"]["Industry"][indval] = array_ind[indval];
    };
    fundobj["Category"]["Industry"]["filter"] = true;
    fundobj["Category"]["Industry"]["hidden"] = true;
}
/**
 * @param {*}
 * @description
 * Hooks into App Script getMethod() [Allows for API Call]
 * @returns JSON object
 */
function doGet(e) {
    var output = main();
    return ContentService.createTextOutput(JSON.stringify(output)).setMimeType(ContentService.MimeType.JSON);
}