/**
 * @file
 */

window.onload = function(){
(function ($) {
  /*--------------------------------------- Pop-up Window ---------------------------------------*/
  let modals = document.getElementsByClassName('modal-window');
  let btns = document.getElementsByClassName("modalBtn");
  let spans = document.getElementsByClassName("close");
  let cat = document.querySelectorAll("h2");
  let filterMod = document.querySelector("#filterBegin");

  filterMod.style.display = "";

  function buttonInit(){
    cat.forEach(function(){
      for(let i = 0; i < btns.length; i++){
        btns[i].addEventListener("click", function() {
         modals[i].style.display = "block";
       });
     }
     for(let i = 0 ; i < spans.length; i++){
       spans[i].addEventListener("click", function() {
        modals[i].style.display = "none";
      });
    }
  });
  }

  /*--------------------------------------- Searchbox Filter ---------------------------------------*/
    //Search Box
    let searchBar = document.querySelector("#search");
    let searchBtn = document.querySelectorAll("#searchBtn");
    let products = document.getElementsByClassName("product-vert");
    let categories = document.getElementsByClassName("filter-defaultcategory");

    function search(item, optionTitle){
      //if the search input is not empty
      if(item != null) {
        //Show all products
        for(let i = 0; i < products.length; i++){
          products[i].style.display = "none";
        }

        if(!optionTitle){
          let str = item.toUpperCase();

          for(let prodIndex = 0; prodIndex < products.length; prodIndex++){
            let title = products[prodIndex].getElementsByClassName("title");
            let info = products[prodIndex].getElementsByClassName("info");
            let header = products[prodIndex].querySelector("h2").textContent.toUpperCase();

            for(let titleIndex = 0; titleIndex < title.length; titleIndex++){
             let prodTitle = title[titleIndex].textContent.toUpperCase();
             let prodInfo = info[titleIndex].textContent.toUpperCase();

             if(prodTitle.indexOf(str) > -1 || prodInfo.indexOf(str) > -1 || header.indexOf(str) > -1) {
              products[prodIndex].style.display = "";
            }
          }
        }
      } else {
        let str = item.toUpperCase().split(", ");
        let title = optionTitle.toUpperCase();

          //searching for terms in headers
          for(let catIndex = 0; catIndex < categories.length; catIndex++){
            let catProducts = categories[catIndex].getElementsByClassName("product-vert");
            let emptyCat = true;
            let inTitle = false;
              //checking each product in current category
              for(let prodIndex = 0; prodIndex < catProducts.length; prodIndex++){
                let prodTitle = catProducts[prodIndex].getElementsByClassName("title");
                let prodInfo = catProducts[prodIndex].getElementsByClassName("info");
                if(catProducts[prodIndex].style.display == "none") {
                    //checking each string in current paragraph
                    for(let titleIndex = 0; titleIndex < prodTitle.length; titleIndex++){
                      for(let strIndex = 0; strIndex < str.length; strIndex++){
                        let parseTitle = prodTitle[titleIndex].textContent.toUpperCase();
                        let parseInfo = prodInfo[titleIndex].textContent.toUpperCase();
                        if(parseTitle.indexOf(title) > -1 && parseInfo.indexOf(str[strIndex]) > -1) {
                          catProducts[prodIndex].style.display = "";
                          emptyCat = false;
                        }
                      }
                    }
                  }
                }
              //for entire categories (Headers or h2)
              let header = categories[catIndex].querySelector("h2").innerHTML.toUpperCase();
              //checking to see if string is in Category Title
              for(let strIndex = 0; strIndex < str.length; strIndex++){
                if(header.indexOf(str[strIndex]) > -1){
                  inTitle = true;
                }
              }

              //if string is found in title
              if(inTitle == true){
                for(let prodIndex = 0; prodIndex < catProducts.length; prodIndex++){
                  catProducts[prodIndex].style.display = "";
                  emptyCat = false;
                }
              }
              //hiding category if empty
              if(emptyCat == true){
                categories[catIndex].style.display = "none";
              } else {
                categories[catIndex].style.display = "";
              }

              //reset check for status of entire category
              inTitle = false;
              emptyCat = true;
            }
          }
        }
      }

      function searchBarFilter(){
        let searchTitleArr = [];

        if(searchBar){
          searchBar.addEventListener("change", function(){
            let searchTerm = searchBar.value;
            search(searchTerm, "");
          });
        }
      }

      /*--------------------------------------- DropDown Menu Filter ---------------------------------------*/
      let dropDown = document.querySelectorAll("select");
      let initOptions = [];

      function dropDownSearch(){
        for(let i = 0; i < dropDown.length; i++){
          initOptions.push(dropDown[i][0].value);
        }

        for(let i = 0; i < dropDown.length; i++){
          dropDown[i].addEventListener("change", function(){
            let option = dropDown[i].value;
            search(option, initOptions[i]);

          //clearing other dropdown fields
          for(let j = 0; j < dropDown.length; j++){
            if(j != i){
              dropDown[j].value = dropDown[j][0].text;
            }
          }
        });
        }
      }
      /*--------------------------------------- Clear Button ---------------------------------------*/
      let clear = document.querySelector("#clear");


      function reset(){
        let none = "";
        if(clear){
          clear.addEventListener("click", function(){
            for(let i = 0; i < products.length; i++){
              products[i].style.display = "";
            }
            for(let i = 0; i < categories.length; i++){
              categories[i].style.display = "";
            }
            searchBar.value = "";
          //clearing all dropdowns fields
          for(let i = 0; i < dropDown.length; i++){
            dropDown[i].value = dropDown[i][0].text;
          }
        });
        }
      }

      /*--------------------------------------- On Start Up ---------------------------------------*/
  function startUp(){
    buttonInit();
    searchBarFilter();
    dropDownSearch();
    reset();
  }

  console.log("Its Linked")

  startUp();
})(jQuery);
};
