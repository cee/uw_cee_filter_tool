# Filter Functions
Contains search function, button initializer function, and searchbar and dropdown functions that run the search function
## buttonInit()
Used for initializing all the buttons and modals on the page
```javascript
/**Initializes all thumbnail buttons to open the modal window with more info
  *
  * @return {NULL} only initializes buttons with onclick functions
 */
```

## search()
Used for product filtering.
```javascript
/**Return array of products which meet critieria through process of elimination
 /**Outputs all 'products' that have the searchterm on the page by changing the display property from "none" to ""
  * @param {string} item item to be searched for
  * @param {string} optionTitle the item title that the item will be under
  *
  * @returns {NULL} changes display to be shown rather than none
  */
```

## searchBarFilter()
Used for running search() on search term in search searchBar
```javascript
/**Posts all 'products' that have the searchterm from the SEARCHBAR on the page
 *
 * @returns {NULL} runs the search function
 */
```

## dropDownSearch()
Used for initializing all drop-downs and running search() on the selected filtering parameter
```javascript
/**Posts all 'products' that have the searchterm from the DROPDOWNS on the page
 *
 * @returns {NULL} runs the search function
 */
```

## clear()
Used for displaying all products on the page and clearing all search parameters in drop-downs and search bar
```javascript
/** Resets all products on the page
 *
 * @returns {NULL} outputs all products
 */
 ```

## startUp()
Runs all functions on page start up
```javascript
/**runs all functions on startup
 *
 * @returns {NULL}
 */
```
