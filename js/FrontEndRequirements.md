# Front End Requirements
## Look and Feel
Needs to be consistent with waterloo site look. Example: https://uwaterloo.ca/hire/about/people

Needs to be able to tile and display correctly on mobile and desktop. Design needs to be responsive.

Animations need to smooth and should adhear to accessibility.

Should fit into main body of site (under banner and right of left column).
## Functionality 
1. On page-load HTML will be organized based on a default category. Javascripts should remove groups and organize all items alphabetically
2. A filter sections should be generated based on categories and groups in JSON data file
3. All links should become clickable buttons and a "more info button" should be generated for each product
4. In the case of multiple images for each product, first image should be the only one displayed
5. On click "more info button" a modal should appear with all info associated with that specific product

## Misc. Info
All elements with the hidden attribute set to true should not appear in the initial product list but should appear in an modal when "more info button" is clicked

## Allowed Tools & Frameworks
* JavaScript (must be IE 11 compliant) ES5
* CSS3
* HTML(never explicitley, must insert using js)
* Jquery 1.8.3
* Jquery UI

## Getting Test Files
1. Go to test_files/jsontohtml.php
2. Copy code into online php compiler
3. Copy generated html into a new html doc
4. Manually include Jquery, Jquery UI, and js/filter.js
5. **OPTIONAL** go to uwaterloo.ca/hire website and insert generated html into appropriate body